"""Middleware Connector Library.

You can find more a thorough description and example implementations about this module at:
https://gitlab.com/companion-to-the-earth/lib-middleware-connector/-/tree/master
"""

# This program has been developed by students from the bachelor Computer
# Science at Utrecht University within the Software Project course. ©️ Copyright
# Utrecht University (Department of Information and Computing Sciences)

