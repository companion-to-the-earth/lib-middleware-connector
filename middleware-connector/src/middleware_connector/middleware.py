"""Middleware interface that enables services to easily communicate with middleware"""

import signal
import sys
import time
import threading
import pika
import logging
from functools import partial

# A list of exceptions that may be caused by a broken connection.
# More info on all pika exceptions:
# https://pika.readthedocs.io/en/stable/modules/exceptions.html
CONNECTION_EXCEPTIONS = (
    AttributeError,
    AssertionError,
    KeyError,
    ConnectionError,
    BrokenPipeError,
    ConnectionAbortedError,
    ConnectionRefusedError,
    ConnectionResetError,
    pika.exceptions.AMQPChannelError,
    pika.exceptions.AMQPConnectionError,
    pika.exceptions.AMQPError,
    pika.exceptions.ChannelClosed,
    pika.exceptions.ChannelClosedByBroker,
    pika.exceptions.ChannelClosedByClient,
    pika.exceptions.ChannelError,
    pika.exceptions.ChannelWrongStateError,
    pika.exceptions.ConnectionBlockedTimeout,
    pika.exceptions.ConnectionClosed,
    pika.exceptions.ConnectionClosedByBroker,
    pika.exceptions.ConnectionClosedByClient,
    pika.exceptions.ConnectionOpenAborted,
    pika.exceptions.ConnectionWrongStateError,
    pika.exceptions.ConsumerCancelled,
    pika.exceptions.DuplicateGetOkCallback,
    pika.exceptions.NackError,
    pika.exceptions.NoFreeChannels,
    pika.exceptions.ProbableAccessDeniedError,
    pika.exceptions.StreamLostError,
    pika.exceptions.UnroutableError
)

def handle_sigterm(*_):
    """Handle SIGTERM triggered by docker gracefully"""
    sys.exit()

# Add SIGTERM callback
signal.signal(signal.SIGTERM, handle_sigterm)

# Setup logging
log = logging.getLogger('middleware-connector')
formatter = logging.Formatter(fmt="%(asctime)s %(levelname)s: %(message)s", 
                          datefmt="%Y-%m-%d - %H:%M:%S")
stream_handler = logging.StreamHandler(sys.stdout)
stream_handler.setLevel(logging.DEBUG)
stream_handler.setFormatter(formatter)
log.addHandler(stream_handler)

class Middleware:
    """Abstracts communication with RabbitMQ and supports multithreaded applications,
    provided that only public functions are called.

    Each thread is assigned a connection. That connection is never shared with any other
    threads, since pika is not thread-safe. Pika made this choice in order to improve
    networking performance.

    It is highly adviced to instantiate a long-lived python object in order to avoid
    closing the TCP connection. This is a highly recommend practice:
    https://www.cloudamqp.com/blog/2018-01-19-part4-rabbitmq-13-common-errors.html
    """

    MAX_CONNECTION_ATTEMPTS = 40

    __connections = {}
    __channels = {}
    __connection_params = None

    #
    # Connection
    #
    def __init__(self, connection_params):
        """Instantiate middleware. Connects to message broker when called.

        :param pika.ConnectionParameters connection_params: Params used to create the connection.
        """

        self.__connection_params = connection_params
        self.__connect()

    # Try to connect to middleware until the service succeeds
    def __connect(self):
        """Connect this thread to an AMQP broker. It raises a ConnectionError
        if it was unable to connect after reaching MAX_CONNECTION_ATTEMPTS.
        """

        thread_id = threading.get_ident()
        attempt = 0

        while True:
            try:
                connection = self.__create_connection(thread_id)
                channel = self.__create_channel(thread_id, connection)
                return channel
            except CONNECTION_EXCEPTIONS:
                if attempt < self.MAX_CONNECTION_ATTEMPTS:
                    attempt += 1
                    warning = f"""{thread_id}: Could not establish TCP connection (attempt {attempt})."""
                    log.warning(warning)
                    time.sleep(1)
                else:
                    raise

    def __create_connection(self, thread_id):
        """Create a TCP connection with middleware.

        :param int: thread_id
        """

        log.info(f'{thread_id}: Creating TCP connection.')
        thread_connection = pika.BlockingConnection(self.__connection_params)
        log.info(f'{thread_id}: Established TCP connection.')

        self.__connections[thread_id] = thread_connection
        return thread_connection

    def __create_channel(self, thread_id, thread_connection):
        """Create a channel for this thread.

        :param int thread_id: the id of the current thread.
        :param pika.BlockingConnection: the connection of the current thread.
        """

        log.info(f'{thread_id}: Started channel creation.')
        thread_channel = thread_connection.channel()
        log.info(f'{thread_id}: Channel opened.')
        self.__channels[thread_id] = thread_channel

        return thread_channel

    def __get_connection(self, thread_id):
        """Return a TCP connection. The connection will be created if it does
        not exist yet. It will be opened if it was closed.

        :param int: thread_id.
        """

        if not self.__connections.__contains__(thread_id):
            return self.__create_connection(thread_id)

        thread_connection = self.__connections[thread_id]

        if not thread_connection.is_open:
            return self.__create_connection(thread_id)

        return thread_connection

    def __get_channel(self, thread_id):
        """Returns the channel for this thread. If it does exits yet or the channel
        was closed, the connection is created again.

        :param int: thread_id
        """

        thread_connection = self.__connections[thread_id]

        if not thread_connection.is_open:
            raise ConnectionError('Attempting to create channel without a TCP connection.')

        if not self.__channels.__contains__(thread_id):
            # Create a channel for this thread if it did not exist.
            return self.__create_channel(thread_id, thread_connection)

        thread_channel = self.__channels[thread_id]

        if not thread_channel.is_open:
            return self.__create_channel(thread_id, thread_connection)

        return thread_channel

    #
    # PUBLISH
    #
    def publish(self, queue_name, message, reply_to='', correlation_id=''):
        """Publish message to a queue. If a connection is missing, attempt
        to connect and try to publish again.

        :param str queue_name: the queue name  to publish the message to.
        :param str message: the message to publish to the queue.
        :param str reply_to: the channel to reply to.
        :param str correlation_id: the id that you will receive on callback.

        """            

        def attempt_to_publish(thread_id, channel, queue_name, message, properties):
            channel.queue_declare(queue_name)
            channel.basic_publish(
                exchange='',
                routing_key=queue_name,
                body=message,
                properties=properties
            )

            log.info(f'{thread_id}: Sent message to {queue_name}: {message}')

        thread_id = threading.get_ident()

        properties = pika.BasicProperties(
            reply_to=reply_to,
            correlation_id=correlation_id
            )
        
        try:
            channel = self.__get_channel(thread_id)
            attempt_to_publish(thread_id, channel, queue_name, message, properties)
        except CONNECTION_EXCEPTIONS:
            channel = self.__connect()
            attempt_to_publish(thread_id, channel, queue_name, message, properties)

        return 0

    #
    # CONSUME
    #
    # pylint: disable=W0613
    @classmethod
    def on_message_received(cls, callback, method, properties, body):
        """A callback function that is triggered when a message is received.

        :param callable callback: the callback function passed
        :param str method: the AMQP method to invoke
        :param pika.spec.BasicProperties properties: message properties
        :param bytes|str body: the message body

        """
        raise NotImplementedError()

    def consume(self, queue_name, stay_open):
        """Start consuming messages on a queue. If a connection is missing,
        attempt to connect and try to consume again.

        :param str queue_name: the queue_name to be consumed.
        :param bool stay_open: close or maintain the connection after a consumption.

        """

        def attempt_to_consume(thread_id, channel, queue_name, stay_open):
            channel.queue_declare(queue_name)

            self.__initialize_consume(queue_name, stay_open)

            log.info(f'{thread_id}: Consuming messages at {queue_name}')
            channel.start_consuming()

        thread_id = threading.get_ident()

        try:
            channel = self.__get_channel(thread_id)
            attempt_to_consume(thread_id, channel, queue_name, stay_open)
        except CONNECTION_EXCEPTIONS:
            channel = self.__connect()
            attempt_to_consume(thread_id, channel, queue_name, stay_open)

        return 0

    def __initialize_consume(self, queue_name, stay_open):
        """consume method with RCP callback

        :param str queue_name: the queue_name to be consumed.
        :param bool stay_open: close or maintain the connection after a consumption.

        """
        def on_response(callback, method, properties, body, stay_open=False):
            """Callback for when a message is received. It triggers the on_message_received
            callback function."""

            thread_id = threading.get_ident()
            channel = self.__get_channel(thread_id)

            # Acknowledge the message, because it was received
            sending_multiple = False
            channel.basic_ack(method.delivery_tag, sending_multiple)
            message = str(body)

            log.info(f'{thread_id}: Received response: {message}')
            self.on_message_received(callback, method, properties, body)

            if not stay_open:
                channel.stop_consuming()

        # define the on_response function that is called when consuming a message
        self.__get_channel(threading.get_ident()).basic_consume(
            queue=queue_name,
            on_message_callback=partial(on_response, stay_open=stay_open),
            auto_ack=False
        )

# This program has been developed by students from the bachelor Computer
# Science at Utrecht University within the Software Project course. ©️ Copyright
# Utrecht University (Department of Information and Computing Sciences)

