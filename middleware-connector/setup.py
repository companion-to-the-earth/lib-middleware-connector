import setuptools

setuptools.setup(
    name="middleware-connector",
    version="2.0.4",
    description="AMQP client library",
    packages=setuptools.find_packages(where="src"),
    package_dir={"":"src"},
    install_requires=[
        "pika~=1.2.0"
    ],
    python_requires=">=3.8",
)