# Utrecht Companion To The Earth - middleware-db

## usage
middleware-connector is a library to be used by our services such that they can easily 
communicate with the middleware.

## functions
<pre>
**src/lib_middleware.py**
- connect(self) \
    Attempts a connection with the middleware until \
    one has been established.
- publish(self, topic, message, reply_to) \
    Publishes a message to a topic with the default \
    reply_to value set to ''.
- declare_publish_queue(self, queue, passive) \
    Creates a queue for messages to be published on. \
    The default value of passive is set to False. \
    declare_publish_queue raises a TypeError if queue is not a string.
- clean_queue(self, queue)
    Removes given queue. \
    Raises a TypeError is queue is not in string format.
- consume_publish_subscribe(self, ch, method, properties, body)
    For now, this function is a stubb, as no services are in need \
    of subscribtions as of yet. Currently only closes connection.
- consume_remote_procedure_call(self, method, properties, body)
    For now, this function is a stubb, as no services are in need \
    of remote procedure calls as of yet. Currently only closes connection.
- close_channel(self)
    Closes connection with middleware.
- close_and_reconnect(self)
    Closes connection with middleware and attempts a new connection.
- consume(self, topic)
    Declares a passive publish channel for the specified topic and \
    consumes incoming messages.
- get_messages_in_queue(self)
    Returns the how many messages may be retrieved from the channel.
</pre>

## used resources
# libraries
- pika: <https://pika.readthedocs.io/en/stable/modules/index.html>

## integration tests
Included in this image is the file 'itest_middleware.py'. This file contains a few tests to make sure the library is working properly. These tests include:
- Connecting to middleware
- Declaring an incorrect publish queue and checking for errors
- Deleting a queue which is not in string format and checking for errors
- Send numerous messages to middleware, verify the received amount of \  messages and check if cleaning the queue removes all messages
- Verifying if valid messages are consumed from the queue
