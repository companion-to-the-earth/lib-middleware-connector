"""tests file for our middleware library"""
import unittest
import threading
from time import sleep
from pika import ConnectionParameters
from src.middleware_connector.middleware import Middleware

class SingleThreadedMiddleware(Middleware):
    """Testing implementation of Middleware which uses the correlation_id's
        of received messages"""

    response = ''

    def on_message_received(self, callback, method, properties, body):
        """Callback for the publish_subscribe pattern"""

        self.response = str(body, 'utf-8')

class MultiThreadedMiddleware(Middleware):
    """Testing implementation of Middleware which uses the correlation_id's
        of received messages"""

    responses = []
    rpc_answers = {}

    def on_message_received(self, callback, method, properties, body):
        """Callback for the publish_subscribe pattern"""

        self.responses.append(str(body, 'utf-8'))

class TestMiddleware(unittest.TestCase):
    """Test class that test middleware library"""

    test_succes_publish_subcribe = True
    test_succes_rcp = True

    def test_single_threaded_publish_consume(self):
        """Test single-threaded networking"""

        params = ConnectionParameters(host='middleware')
        middleware = SingleThreadedMiddleware(params)
        queue = 'single_threaded_publish_consume'

        def test_publish_subcribe(answer):
            """Test procedure for publish_subscribe pattern"""

            self.assertEqual(middleware.publish(queue, answer), 0)
            self.assertEqual(middleware.consume(queue, False), 0)
            self.assertEqual(middleware.response, answer)

        test_publish_subcribe('hello world')
        test_publish_subcribe('geodude')
        test_publish_subcribe('The third message sent')

    def test_single_threaded_timeout(self):
        """Test whether the systems can restore communication after the connection was closed."""

        # Set hearthbeat to 3 seconds in order to test the timeout
        params = ConnectionParameters(
            host='middleware',
            heartbeat=3
            )
        middleware = SingleThreadedMiddleware(params)
        queue = 'test_single_threaded_timeout'

        def test_remote_procedure(answer):
            """Test procedure for remote_procedural_call pattern"""

            self.assertEqual(middleware.publish(queue, answer, queue), 0)
            self.assertEqual(middleware.consume(queue, False), 0)
            self.assertEqual(middleware.response, answer)

        test_remote_procedure('this is a timeout test')
        # Force a time-out by missing 2 heartbeats
        sleep(20)

        # Make sure the server does not crash when closing an already closed channel
        test_remote_procedure('is this message still received after the timeout?')

    def test_multi_threaded_publish_consume(self):
        """Test multi-threaded networking"""

        def publish_consume(middleware):
            thread_id = threading.get_ident()
            queue = f'multi_threaded_publish_consume-{thread_id}'

            def test_publish_subcribe(answer):
                """Test procedure for publish_subscribe pattern"""

                try:
                    self.assertEqual(middleware.publish(queue, answer), 0)
                    self.assertEqual(middleware.consume(queue, False), 0)
                    self.assertIn(answer, middleware.responses)
                except AssertionError as exception:
                    self.test_succes_publish_subcribe = False
                    raise exception

            test_publish_subcribe(f'hello world-{thread_id}')
            test_publish_subcribe(f'geodude-{thread_id}')
            test_publish_subcribe(f'The third message sent-{thread_id}')

        params = ConnectionParameters(host='middleware')
        middleware = MultiThreadedMiddleware(params)

        # initialise threads
        thread_count = 20
        threads = [None] * thread_count

        for i in range(thread_count):
            threads[i] = threading.Thread(target=publish_consume, args=([middleware]))

        # start and join threads
        for thread in threads:
            thread.start()
        for thread in threads:
            thread.join()

        self.assertTrue(self.test_succes_publish_subcribe)

# This program has been developed by students from the bachelor Computer
# Science at Utrecht University within the Software Project course. ©️ Copyright
# Utrecht University (Department of Information and Computing Sciences)
