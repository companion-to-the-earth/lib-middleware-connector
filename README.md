# Middleware Connector Library
Middleware Connector library module is a common interface for all microservices. It is based on the pika module implementation which is a pure-Python implementation of the AMQP 0-9-1 protocol that tries to stay fairly independent of the underlying network support library.

Everything revolving around connections, channels and queues is abstracted, so that you only have to focus on publishing and consuming messages.

## Table of Contents
1. [Dependencies](#dependencies)
2. [Multi-threading](#Multi-threading)
3. [API](#API)
4. [Examples](#Examples)
    * [NiceDayService (single-threaded)](#NiceDayService_(single-threaded))
    * [PrintService (multi-threaded)](#PrintService_(multi-threaded))

## Dependencies
The framework is built upon the pure-Python pika library. While developing this framework we looked at both the development documentation and the source code. Pika is quite a low-level implementation and may be difficult to understand initially, but is is very well documented.

* For more information on Pika: https://pika.readthedocs.io/en/stable/
* For more information on the AMQP protocol: https://www.rabbitmq.com/documentation.html

Make sure to install pika using:
```
pip install pika
```

## Multi-threading
This library supports long-lived connections in multi-threaded environment. Pika explicitly states that its connections and channels are not thread-safe (https://github.com/pika/pika/blob/master/docs/faq.rst).

For that reason, each thread_id gets its own connection with its own channel. Ones the thread is finished, the connection will persist if the middleware object was not instantiated in the thread that just stopped. This enables a next thread with the same thread_id to use the same connection.

Long-lived connections are essential to achieve high-performance:
https://www.cloudamqp.com/blog/2018-01-19-part4-rabbitmq-13-common-errors.html

## API
The API publicly exposes two functions, publish, consume and on_receive_message:
```python
def publish(self, queue_name, message, reply_to='', correlation_id=''):
    """Publish message to a queue. If a connection is missing, attempt
    to connect and try to publish again.

    :param str queue_name: the queue name  to publish the message to.
    :param str message: the message to publish to the queue.
    :param str reply_to: the channel to reply to.
    :param str correlation_id: the id that you will receive on callback.

    """
```
```python
def consume(self, queue_name, stay_open):
    """Start consuming messages on a queue. If a connection is missing,
    attempt to connect and try to consume again.

    :param str queue_name: the queue_name to be consumed.
    :param bool stay_open: close or maintain the connection after a consumption.

    """
```
```python
@classmethod
def on_message_received(cls, callback, method, properties, body):
    """A callback function that is triggered when a message is received.

    :param callable callback: the callback function passed
    :param str method: the AMQP method to invoke
    :param pika.spec.BasicProperties properties: message properties
    :param bytes|str body: the message body

    """
```

## Examples
Lets create two simple services. 
* A single-threaded NiceDayService that wishes the sender of the message it receives a nice day.
* A multi-threaded PrintService that prints every message it receives. 

> Important note: The code below was not tested, so it may contain some bugs. The workflow however, is correct and thorougly described.

### NiceDayService_(single-threaded)
We are first going to create a message handler for NiceDayService by creating a sub-class of middleware.
This essential, since we want to specify a callback for when a message is received.
```python
class NiceDayMiddleware(Middleware):
    """Demo implementation of Middleware that receives a message and simply thanks
    the sender for the message and wishes it a nice day."""

    def on_message_received(self, ch, method, properties, body):
        """A callback for the remote procedural call pattern. The sender of this message expects a
        message back and has provided us with a reply_to queue. This service will wish it a nice day!"""

        queue = properties.reply_to
        self.publish(queue, f'Thank you {queue}, Have a nice day!')
```

We can now create a service around this class called NiceDayService.
```python
# Initialise the NiceDayService, connect to a message broker called 'middleware'
params = pika.ConnectionParameters(host='middleware')
middleware = NiceDayMiddleware(params)

# Consume the queue 'nice_day', so that we can start receiving messages.
# stay_open=True states that we will continue consuming ones we have handled a message.
middleware.consume('nice_day', stay_open=True)
```

### PrintService_(multi-threaded)
Let's create another service called PrintService that can communicate with it. This service will show the multi-threading capabilities of the framework.
```python
import threading

class PrintMiddleware(Middleware):
    """Demo implementation of the middleware that prints all messages it receives"""

    # Maps thread_id to response -> {thread_id, body}.
    __responses = {}

    def on_message_received(self, ch, method, properties, body):
        """A callback that print the received message and stores the message in a dictionary
        where it is mapped to the thread_id."""

        thread_id = thread.get_ident()
        body_string = str(body, 'utf-8')
        print(f'Received {body_string} on thread: {thread_id}')

        # Store the message in the __responses dictionary
        # This way, we can access the response outside of the callback scope.
        self.__responses[thread_id] = body_string

    def get_thread_response(self):
        """Return the response that was received on this thread"""

        thread_id = thread.get_ident()
        response = __responses[thread_id]

        # Remove response immediately, so that no other thread can accidentally read it.
        __responses[thread_id] = None
        return response
```

#### Running threads
Now that we have written a middleware class object that can print the messages it receives and store the messages for each thread, we can spin up some threads to run the program.
```python
# Define the queue name that we will listen to
QUEUE_SELF = 'print'

# Define the queue name of the NiceDayService
QUEUE_NICE_DAY = 'nice_day'

def publish_consume(middleware):
    """A thread_job that publishes and consumes a message. It shows how you can extract a response
    after consuming was stopped."""

    # Create a unique reply_to queue name for this thread.
    # This way, other threads won't receive the response.
    thread_id = threading.get_ident()
    reply_to = f'{QUEUE_SELF}-{thread_id}'

    # Send a message using the rcp protocol, this means that a response will be send to reply_to.
    middleware.publish(QUEUE_NICE_DAY, answer, reply_to)

    # In this case, reply_to is our thread-queue
    # Start consuming the reply_to queue in order to extract the response.
    middleware.consume(reply_to, False)

    # You can now do whatever you want with the response
    response = middleware.get_thread_response()

# Initialise the PrintService, connect to a message broker called 'middleware'
params = pika.ConnectionParameters(host='middleware')
middleware = PrintMiddleware(params)

# Initialise threads
THREAD_COUNT = 10
threads = [None] * THREAD_COUNT

for i in range(THREAD_COUNT):
    threads[i] = threading.Thread(target=publish_consume, args=([middleware]))

# Run the first set of threads
for thread in threads:
    thread.start()
for thread in threads:
    thread.join()
```

#### Reusing the connections
If we run a second set of threads and those threads have the same thread_id as the previous threads then the connections for those threads will be reused. This is because the middleware object is a persistent long-lived object if it is created outside the thread. 

Long-lived connections are crucial for achieving high performance.
